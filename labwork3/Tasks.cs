﻿/*------------------PaDC-----------------------------
---------------Labwork #3----------------------------
------------Threads in C#----------------------------
--Group IO-52----------------------------------------
--Author: Butskiy Yuriy------------------------------
--Function 1 d = ((A*B) + (C*(B*(MA*MD))))-----------
--Function 2 MK = (TRANS(MA) * TRANS(MB * MM) + MX)--
--Function 3 T = (SORT(O + P) * TRANS(MR * MS)-------
--06.10.2017-----------------------------------------*/

using System;
using System.Threading;

namespace labwork3
{
    class Tasks
    {
        private int N;
        private Data data;

        public Tasks(int N)
        {
            this.N = N;
            data = new Data(N);
        }

        public void T1()
        {
            Console.WriteLine("Task #1 started");
            Thread.Sleep(2000);
            int[] A = new int[N];
            int[] B = new int[N];
            int[] C = new int[N];
            int[,] MA = new int[N, N];
            int[,] MD = new int[N, N];
            data.Vector_Input(A);
            data.Vector_Input(B);
            data.Vector_Input(C);
            data.Matrix_Input(MA);
            data.Matrix_Input(MD);
            int d = data.F1(A, B, C, MA, MD);
            Thread.Sleep(1000);
            if (N < 10)
            {
                Console.WriteLine("T1: d = " + d);
            }
            Console.WriteLine("Task #1 finished");
        }

        public void T2()
        {
            Console.WriteLine("Task #2 started");
            Thread.Sleep(2000);
            int[,] MA = new int[N, N];
            int[,] MB = new int[N, N];
            int[,] MM = new int[N, N];
            int[,] MX = new int[N, N];
            data.Matrix_Input(MA);
            data.Matrix_Input(MB);
            data.Matrix_Input(MM);
            data.Matrix_Input(MX);
            int[,] MK = data.F2(MA, MB, MM, MX);
            Thread.Sleep(2000);
            if (N < 10)
            {
                Console.WriteLine("T2: MK = ");
                data.Matrix_Output(MK);
            }
            Console.WriteLine("Task #2 finished");
        }

        public void T3()
        {
            Console.WriteLine("Task #3 started");
            Thread.Sleep(2000);
            int[] O = new int[N];
            int[] P = new int[N];
            int[,] MR = new int[N, N];
            int[,] MS = new int[N, N];
            data.Vector_Input(O);
            data.Vector_Input(P);
            data.Matrix_Input(MR);
            data.Matrix_Input(MS);
            int[] T = data.F3(O, P, MR, MS);
            Thread.Sleep(3000);
            if (N < 10)
            {
                Console.WriteLine("T3: T = ");
                data.Vector_Output(T);
            }
            Console.WriteLine("Task #3 finished");
        }
    }
}
