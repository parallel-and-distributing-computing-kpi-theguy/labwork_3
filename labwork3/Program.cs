﻿/*------------------PaDC-----------------------------
---------------Labwork #3----------------------------
------------Threads in C#----------------------------
--Group IO-52----------------------------------------
--Author: Butskiy Yuriy------------------------------
--Function 1 d = ((A*B) + (C*(B*(MA*MD))))-----------
--Function 2 MK = (TRANS(MA) * TRANS(MB * MM) + MX)--
--Function 3 T = (SORT(O + P) * TRANS(MR * MS)-------
--06.10.2017-----------------------------------------*/

using System;
using System.Threading;

namespace labwork3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter N:");
            int N = Int32.Parse(Console.ReadLine());
            Tasks task = new Tasks(N);
            Thread Th1 = new Thread(task.T1);
            Thread Th2 = new Thread(task.T2);
            Thread Th3 = new Thread(task.T3);
            Th1.Priority = ThreadPriority.Lowest;
            Th2.Priority = ThreadPriority.Highest;
            Th3.Priority = ThreadPriority.Normal;
            Th1.Start();
            Th2.Start();
            Th3.Start();
            Th1.Join();
            Th2.Join();
            Th3.Join();
            Console.WriteLine("Main class finished");
            Console.ReadKey();
        }
    }
}
