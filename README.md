Laboratory work #3 for Parallel and Distributing Computing.

Purpose for the work: To study and practice how to create threads in programming language - C#.
Programming language: C#
Used technologies: In C# multithreading works as class Thread, that use as argument the name of function, that would work separately on the one of the threads of CPU. To start the calculation of that function, object of the class must call function Start(), after that it will work as multithread program.

Controling of the program:
variable N stands for the size of Matrixes and Vectors respectively.
Program works for 3 threads, all returns the result.